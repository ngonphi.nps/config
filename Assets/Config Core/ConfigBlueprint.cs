using System.Collections.Generic;

namespace com.unimob.config.core
{
    public class ConfigBlueprint : IConfigBlueprint
    {
        private List<string> intKeys;
        private List<string> floatKeys;
        private List<string> stringKeys;
        private List<string> boolKeys;
        private Dictionary<string, object> data;

        public ConfigBlueprint()
        {
            intKeys = new List<string>();
            floatKeys = new List<string>();
            stringKeys = new List<string>();
            boolKeys = new List<string>();
            data = new Dictionary<string, object>();
        }

        private IConfigBlueprint Set<T>(IEnumerable<string> Keys, IList<string> keys, string id, T value)
        {
            foreach (string key in Keys)
            {
                if (key == id)
                {
                    data[id] = value;
                    return this;
                }
            }

            keys.Add(id);
            data.Add(id, value);
            return this;
        }

        private T Get<T>(IEnumerable<string> Keys, string key)
        {
            foreach (string id in Keys)
            {
                if (key == id)
                {
                    return (T)data[id];
                }
            }

            return default;
        }

        #region Implement IConfigBlueprint

        public IEnumerable<string> IntKeys { get => intKeys; }

        public IEnumerable<string> FloatKeys { get => floatKeys; }

        public IEnumerable<string> StringKeys { get => stringKeys; }

        public IEnumerable<string> BoolKeys { get => boolKeys; }

        public IDictionary<string, object> Export()
        {
            return data;
        }

        public bool GetBool(string key)
        {
            return Get<bool>(BoolKeys, key);
        }

        public float GetFloat(string key)
        {
            return Get<float>(FloatKeys, key);
        }

        public int GetInt(string key)
        {
            return Get<int>(IntKeys, key);
        }

        public string GetString(string key)
        {
            return Get<string>(StringKeys, key);
        }

        public IConfigBlueprint SetBool(string id, bool value)
        {
            return Set(BoolKeys, boolKeys, id, value);
        }

        public IConfigBlueprint SetFloat(string id, float value)
        {
            return Set(FloatKeys, floatKeys, id, value);
        }

        public IConfigBlueprint SetInt(string id, int value)
        {
            return Set(IntKeys, intKeys, id, value);
        }

        public IConfigBlueprint SetString(string id, string value)
        {
            return Set(StringKeys, stringKeys, id, value);
        }

        #endregion
    }
}