using System;
using System.Threading.Tasks;

namespace com.unimob.config.core
{
    public interface IConfigProvider
    {
        event Action OnFetchSuccess;
        event Action OnFetchError;
        event Action OnSetDefaultComplete;

        IConfigValue GetValue(string id);
        Task SetDefaultValues(IConfigBlueprint config);
        void Fetch();
    }
}