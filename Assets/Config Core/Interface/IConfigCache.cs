namespace com.unimob.config.core
{
    public interface IConfigCache
    {
        void Cache(IConfigBlueprint blueprint);
        void Load(IConfigBlueprint blueprint);
    }
}