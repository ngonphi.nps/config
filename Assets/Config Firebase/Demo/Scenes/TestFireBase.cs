using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.unimob.config.core;
using com.unimob.config.firebase;
using Firebase;
using Firebase.Extensions;
using System;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

public class TestFireBase : MonoBehaviour
{
    private IConfigProvider provider;

    private void Start()
    {
        var cache = new PlayerPrefCache();
        provider = new FirebaseConfigProvider(cache);

        provider.OnSetDefaultComplete += OnSetDefaultComplete;
        provider.OnFetchSuccess += OnFetchSuccess;
        provider.OnFetchError += OnFetchError;

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void OnFetchError()
    {
        Debug.Log("Test Fetch Error!");
    }

    private void OnFetchSuccess()
    {
        Debug.Log("Test Fetch Success!");

        // ?
        //Debug.Log(PlayerPrefs.GetString(RemoteConfigKey.ab_demo1.ToString(), "null"));
        //Debug.Log(PlayerPrefs.GetInt(RemoteConfigKey.ab_demo2.ToString(), -1));
        //Debug.Log(PlayerPrefs.GetInt(RemoteConfigKey.ab_demo3.ToString(), 1));
        //Debug.Log(PlayerPrefs.GetFloat(RemoteConfigKey.ab_demo4.ToString(), -1.0f));

        Debug.Log(provider.GetValue(RemoteConfigKey.ab_demo1.ToString()).String);
    }

    private void OnSetDefaultComplete()
    {
        Debug.Log("Test Set Default Complete!");

        provider.Fetch();
    }

    private void OnDestroy()
    {
        provider.OnSetDefaultComplete -= OnSetDefaultComplete;
        provider.OnFetchSuccess -= OnFetchSuccess;
        provider.OnFetchError -= OnFetchError;
    }

    private void InitializeFirebase()
    {
        var defaultConfig = LoadRemoteConfig();

        var keys = defaultConfig.Export().Keys;
        CreateRemoteConfigKeyFile(keys);

        //foreach (var item in defaultConfig.Export())
        //{
        //    Debug.Log($"{item.Key} : {item.Value}");
        //}

        provider.SetDefaultValues(defaultConfig);
    }

    private IConfigBlueprint LoadRemoteConfig()
    {
        IConfigBlueprint remoteConfig = new ConfigBlueprint();

        var text = Resources.Load<TextAsset>("remote_config");
        dynamic data = JObject.Parse(text.text);
        Dictionary<string, dynamic> values = data.parameters.ToObject<Dictionary<string, dynamic>>();
        foreach (var param in values)
        {
            string key = param.Key;
            string value = param.Value.defaultValue.value;
            string type = param.Value.valueType;

            switch (type)
            {
                case "STRING":
                    remoteConfig.SetString(key, value);
                    break;
                case "BOOLEAN":
                    remoteConfig.SetBool(key, value.CompareTo("true") == 0 ? true : false);
                    break;
                case "NUMBER":
                    if (value.Contains("."))
                        remoteConfig.SetFloat(key, float.Parse(value));
                    else
                        remoteConfig.SetInt(key, int.Parse(value));
                    break;
            }
        }

        return remoteConfig;
    }
    private void CreateRemoteConfigKeyFile(ICollection<string> lines)
    {
#if UNITY_EDITOR
        string file_path = @"Assets/RemoteConfigKey.cs";
        try
        {
            string content = @"namespace com.unimob.config.firebase @{ @public enum RemoteConfigKey@{@";

            foreach (var line in lines)
                content += $"{line},@";
            content += "}@}";

            content = content.Replace("@", Environment.NewLine);

            using (FileStream fs = File.Create(file_path))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(content);
                fs.Write(info, 0, info.Length);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
#endif
    }
}