using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

using com.unimob.config.core;

using Firebase;
using Firebase.RemoteConfig;
using Firebase.Extensions;

namespace com.unimob.config.firebase
{
    public class FirebaseConfigProvider : IConfigProvider
    {
        private IConfigCache cache;
        private IConfigBlueprint defaultConfig;

        public FirebaseConfigProvider()
        {

        }

        public FirebaseConfigProvider(IConfigCache cache)
        {
            this.cache = cache;
        }

        #region Implement IConfigProvider

        public event Action OnFetchSuccess;
        public event Action OnFetchError;
        public event Action OnSetDefaultComplete;

        public void Fetch()
        {
            FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero).ContinueWithOnMainThread(FetchComplete);
        }

        private void FetchComplete(Task fetchTask)
        {
            var info = FirebaseRemoteConfig.DefaultInstance.Info;
            switch (info.LastFetchStatus)
            {
                case LastFetchStatus.Success:
                    {
                        FirebaseRemoteConfig.DefaultInstance.ActivateAsync().ContinueWithOnMainThread(task =>
                        {
                            foreach (var key in defaultConfig.StringKeys)
                            {
                                defaultConfig.SetString(key, GetValue(key).String);
                            }

                            foreach (var key in defaultConfig.IntKeys)
                            {
                                defaultConfig.SetInt(key, GetValue(key).Int);
                            }

                            foreach (var key in defaultConfig.FloatKeys)
                            {
                                defaultConfig.SetFloat(key, GetValue(key).Float);
                            }

                            foreach (var key in defaultConfig.BoolKeys)
                            {
                                defaultConfig.SetBool(key, GetValue(key).Boolean);
                            }

                            cache.Cache(defaultConfig);
                        });

                        Debug.Log("Fetch completed successfully!");
                        OnFetchSuccess?.Invoke();
                    }
                    break;
                case LastFetchStatus.Failure:
                    {
                        Debug.Log("Fetch encountered an error.");
                        OnFetchError?.Invoke();
                    }
                    break;
                case LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
        }

        public IConfigValue GetValue(string id)
        {
            IConfigValue value = new FirebaseConfigValue(id);
            return value;
        }

        public Task SetDefaultValues(IConfigBlueprint config)
        {
            cache.Load(config);
            defaultConfig = config;

            var setDefaultTask = FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaultConfig.Export());
            return setDefaultTask.ContinueWithOnMainThread(task =>
            {
                OnSetDefaultComplete?.Invoke();
            });
        }

        #endregion
    }
}