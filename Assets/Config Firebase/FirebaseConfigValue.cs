using com.unimob.config.core;
using Firebase.RemoteConfig;

namespace com.unimob.config.firebase
{
    public readonly struct FirebaseConfigValue : IConfigValue
    {
        private readonly ConfigValue value;

        public FirebaseConfigValue(string id)
        {
            this.value = FirebaseRemoteConfig.DefaultInstance.GetValue(id);
        }

        #region Implement IConfigValue

        public float Float => (float)this.value.DoubleValue;

        public double Double => this.value.DoubleValue;

        public int Int => (int)this.value.DoubleValue;

        public long Long => this.value.LongValue;

        public string String => this.value.StringValue;

        public bool Boolean => this.value.BooleanValue;

        #endregion
    }
}